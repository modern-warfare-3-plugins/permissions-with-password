﻿using Addon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Timers;

namespace Permissions
{
    public class Main : CPlugin
    {
        private Timer                                           _failedTimer;
        private List<string>                                    _userGroups;
        private List<ConfigurationOption>                       _config;

        private bool                                            _useAuthentication;
        private int                                             _coolDownPeriod;
        private TimeSpan                                        _coolDownPeriodTime;
        private int                                             _failedAttemptsAllowed;
        private List<string>                                    _authGroups;
        private readonly Dictionary<string, bool>               _authenticated              = new Dictionary<string, bool>();
        private readonly Dictionary<string, FailedAttempts>     _failedLoginAttempts        = new Dictionary<string, FailedAttempts>();

        /// <summary>
        /// Executes when the MW3 server loads
        /// </summary>
        public override void OnServerLoad()
        {
            ServerPrint("Permission plugin loaded. Author: Pozzuh. Version 1.4");
            ServerPrint("Permission plugin updated by SgtLegend");

            // First thing we need to do is construct the configuration for the permissions plugin
            CheckAndSetPermissionConfig();

            // Next we need to initialize the user groups
            InitUserGroups();

            // Are we using authentication?
            bool.TryParse(GetConfigValue("LoginRequired"), out _useAuthentication);

            // Ensure a password hash was set
            if (GetConfigValue("LoginPassword").Length == 0)
            {
                _useAuthentication = false;
            }

            if (_useAuthentication)
            {
                _authGroups = new List<string>(GetConfigValue("LoginGroups").Split(','));
                _coolDownPeriod = int.Parse(GetConfigValue("CoolDownPeriod"));
                _failedAttemptsAllowed = int.Parse(GetConfigValue("FailedAttempts"));
                _coolDownPeriodTime = TimeSpan.FromMilliseconds(_coolDownPeriod);

                // Create the failed timer
                _failedTimer = new Timer
                {
                    Enabled = true,
                    Interval = 2000
                };
                
                _failedTimer.Elapsed += CheckFailedAttempts;
            }
        }

        /// <summary>
        /// Checks every user that has a locked status other than the min value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckFailedAttempts(object sender, ElapsedEventArgs e)
        {
            var players = _failedLoginAttempts.Where(player => player.Value.Locked);

            foreach (var player in players.Where(player => (DateTime.Now - player.Value.Time) > TimeSpan.Zero))
            {
                player.Value.Locked = false;
                player.Value.Time = DateTime.MinValue;
                player.Value.Total = 0;
            }
        }

        /// <summary>
        /// Executes when a player joins the server
        /// </summary>
        /// <param name="client"></param>
        public override void OnPlayerConnect(ServerClient client)
        {
            if (_authenticated.ContainsKey(client.XUID)) return;

            _authenticated.Add(client.XUID, false);
            _failedLoginAttempts.Add(client.XUID, new FailedAttempts());
        }

        /// <summary>
        /// Executes when a player leaves the server
        /// </summary>
        /// <param name="client"></param>
        public override void OnPlayerDisconnect(ServerClient client)
        {
            _authenticated[client.XUID] = false;
        }

        /// <summary>
        /// Exeutes when a player types something in the chat
        /// </summary>
        /// <param name="message"></param>
        /// <param name="client"></param>
        /// <param name="teamchat"></param>
        /// <returns></returns>
        public override ChatType OnSay(string message, ServerClient client, bool teamchat)
        {
            string returnMessage = null;

            // Get the command
            var split = message.Split(' ');
            
            if (split[0] == null || (split[0] != null && split[0].Length == 0))
            {
                return ChatType.ChatContinue;
            }

            var command = split[0].ToLower();
            var theCommand = command.Substring(1);

            // Get the group that the user is in and the commands allowed by that group
            var userGroup = GetUserGroup(client.XUID);
            var groupCommands = GetGroupValue(userGroup, "commands");

            // Are we using authentication for the users group? If so ensure they are logged in before
            // they can continue
            if (_useAuthentication && _authGroups.Contains(userGroup) && !_authenticated[client.XUID] &&
                command.StartsWith("!"))
            {
                var player = _failedLoginAttempts[client.XUID];

                if (player.Locked)
                {
                    returnMessage =
                        string.Format("You have been locked out from logging in for {0} for too many failed attempts",
                            GetOverallTime(DateTime.Now - player.Time));
                }
                else if (!Regex.Match(theCommand, "^(login|logout)$").Success)
                {
                    returnMessage = "You're not logged in yet, please use ^7!login <password>";
                }

                if (returnMessage != null)
                {
                    TellClient(client.ClientNum, string.Format("[^1Permissions^7] ^:{0}", returnMessage), true);
                    return ChatType.ChatNone;
                }
            }

            // Execute the command
            switch (theCommand)
            {
                case "login":
                case "logout":
                    returnMessage = "You're already logged out, please use ^7!login <password> ^:if you wish to log in";

                    if (!_useAuthentication)
                    {
                        returnMessage = "Authentication is disabled, simply type commands as normal";
                    }
                    else if (!_authGroups.Contains(userGroup))
                    {
                        returnMessage = "You don't need to be logged in, simply type commands as normal";
                    }
                    else if (theCommand == "login")
                    {
                        if (_authenticated[client.XUID])
                        {
                            returnMessage = "You're already logged in, type commands as normal";
                        }
                        else
                        {
                            if (split.Length == 1 || (split.Length == 2 && split[1].Length == 0))
                            {
                                returnMessage = "Usage: ^7!login <password>";
                            }
                            else if (GetMd5Hash(split[1]) == GetConfigValue("LoginPassword"))
                            {
                                _authenticated[client.XUID] = true;

                                ServerPrint(
                                    string.Format(
                                        "[Permissions] ({0:dd/MM/yyyy HH:mm:ss}) {1} aka {2} was logged in successfully",
                                        DateTime.Now, client.XUID, client.Name));

                                returnMessage = "You have been logged in successfully";
                            }
                            else
                            {
                                returnMessage = "The password you entered is incorrect!";

                                // Get the players failed login attempts
                                var player = _failedLoginAttempts[client.XUID];

                                if (++player.Total == _failedAttemptsAllowed)
                                {
                                    returnMessage =
                                        string.Format(
                                            "You have been locked out from logging in for {0} for too many failed attempts",
                                            GetOverallTime(_coolDownPeriodTime));

                                    ServerPrint(
                                        string.Format(
                                            "[Permissions] ({0:dd/MM/yyyy HH:mm:ss}) {1} aka {2} was locked out",
                                            DateTime.Now, client.XUID, client.Name));

                                    player.Locked = true;
                                    player.Time = DateTime.Now + TimeSpan.FromMilliseconds(_coolDownPeriod);
                                }

                                _failedLoginAttempts[client.XUID] = player;
                            }
                        }
                    }
                    else if (theCommand == "logout" && _authenticated[client.XUID])
                    {
                        _authenticated[client.XUID] = false;

                        ServerPrint(string.Format("[Permissions] ({0:dd/MM/yyyy HH:mm:ss}) {1} aka {2} logged out",
                                                  DateTime.Now, client.XUID, client.Name));

                        returnMessage = "You have been logged out";
                    }

                    TellClient(client.ClientNum, string.Format("[^1Permissions^7] ^:{0}", returnMessage), true);
                    return ChatType.ChatNone;

                case "getxuid":
                case "gettype":
                    TellClient(client.ClientNum,
                               (command == "!getxuid"
                                    ? string.Format("Your XUID is: {0}", client.XUID)
                                    : string.Format("Your user type is: {0}", userGroup)), true);

                    return ChatType.ChatNone;

                case "help":
                case "cmdlist":
                    string allowedCommands = "^1You can use: ^7";

                    // Join the commands by a seperated comma
                    allowedCommands += string.Join("^1, ^7", groupCommands.ToArray());

                    // Check the length of the "allowedCommands", if it's total length doesn't exceed 143 characters
                    allowedCommands = ReplaceLastCommand(allowedCommands) + "...";
                    
                    TellClient(client.ClientNum, allowedCommands, true);
                    return ChatType.ChatNone;
            }

            // Check if the player is protected against the given command
            var protectedXuids = GetServerCFG("Permission", "Protected_xuids", "").Split(',');
            var protectedCommands = GetServerCFG("Permission", "Protected_commands", "").Split(',');
            var requestedPlayer = protectedXuids.Length > 0 ? DeterminePlayer(message.ToLower()) : null;

            if (requestedPlayer != null && protectedCommands.Length > 0)
            {
                if (protectedCommands.Any(x => x == command && protectedXuids.Contains(requestedPlayer.XUID)))
                {
                    TellClient(client.ClientNum,
                               string.Format(
                                   "[^1Permissions^7] ^:You aren't allowed to use ^7{0} ^:command against ^7{1}",
                                   command, requestedPlayer.Name), true);

                    return ChatType.ChatNone;
                }
            }

            // Finally check the immune groups against the current user, if the user is trying to use a command
            // they aren't allowed to be using let them know that
            var immuneGroups = GetServerCFG("Permission", "Immune_groups", "Admin").Split(',');

            if (!immuneGroups.Contains(userGroup) && command.StartsWith("!") && !groupCommands.Contains(command))
            {
                TellClient(client.ClientNum, "[^1Permissions^7] ^:You aren't allowed to use that command!", true);
                return ChatType.ChatNone;
            }

            return ChatType.ChatContinue;
        }

        /// <summary>
        /// Sets all the default configuration data then attempts to collect the user configured data
        /// from the sv_config.ini file that exists on the server
        /// </summary>
        private void CheckAndSetPermissionConfig()
        {
            _config = new List<ConfigurationOption>
                {
                    new ConfigurationOption
                        {
                            Name = "Usergroups",
                            Value = "Admin,Moderator,User"
                        },
                    new ConfigurationOption
                        {
                            Name = "Admin_xuids",
                            Value = ""
                        },
                    new ConfigurationOption
                        {
                            Name = "Admin_commands",
                            Value = "*ALL*"
                        },
                    new ConfigurationOption
                        {
                            Name = "Moderator_xuids",
                            Value = ""
                        },
                    new ConfigurationOption
                        {
                            Name = "Moderator_commands",
                            Value = "!help,!getxuid,!gettype"
                        },
                    new ConfigurationOption
                        {
                            Name = "User_xuids",
                            Value = "*EVERYONE*"
                        },
                    new ConfigurationOption
                        {
                            Name = "User_commands",
                            Value = "!help,!getxuid,!gettype"
                        },
                    new ConfigurationOption
                        {
                            Name = "LoginRequired",
                            Value = "false"
                        },
                    new ConfigurationOption
                        {
                            Name = "LoginGroups",
                            Value = "Admin,Moderator"
                        },
                    new ConfigurationOption
                        {
                            Name = "LoginPassword",
                            Value = ""
                        },
                    new ConfigurationOption
                        {
                            Name = "CoolDownPeriod",
                            Value = "300000"
                        },
                    new ConfigurationOption
                        {
                            Name = "FailedAttempts",
                            Value = "3"
                        }
                };

            foreach (var c in _config)
            {
                var configValue = GetServerCFG("Permission", c.Name, "xx__xx");

                if (configValue == "xx__xx")
                {
                    SetServerCFG("Permission", c.Name, c.Value);
                }
                else
                {
                    c.Value = configValue;
                }
            }
        }

        /// <summary>
        /// Retrieves the user group configuration for the permission plugin
        /// </summary>
        private void InitUserGroups()
        {
            _userGroups = new List<string>((GetConfigValue("Usergroups") ?? string.Empty).Split(','));
        }

        /// <summary>
        /// Retrieves the group name for the given player XUID
        /// </summary>
        /// <param name="xuid"></param>
        /// <returns></returns>
        private string GetUserGroup(string xuid)
        {
            if (_userGroups.Count <= 0) return "User";

            foreach (var group in _userGroups)
            {
                var usersInGroup = GetGroupValue(group, "xuids");

                if (usersInGroup.Contains(xuid) || (usersInGroup[0] != null && usersInGroup[0] == "*EVERYONE*"))
                {
                    return group;
                }
            }

            return "User";
        }

        /// <summary>
        /// Retrieves the given value type for the given group name
        /// </summary>
        /// <param name="groupName"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        private List<string> GetGroupValue(string groupName, string type)
        {
            groupName = groupName + "_" + type;

            // Attempt to get the value from the configuration
            var groupPlayers = GetConfigValue(groupName);

            if (groupPlayers != null) return new List<string>(groupPlayers.Split(','));

            // Get the group config value
            groupPlayers = GetServerCFG("Permission", groupName, "");

            _config.Add(new ConfigurationOption
            {
                Name = groupName,
                Value = groupPlayers
            });

            return new List<string>(groupPlayers.Split(','));
        }

        /// <summary>
        /// Attempts to return the configuration string for the given configuration name, if it
        /// can't find the value it will simply return a null value
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        private string GetConfigValue(string name)
        {
            var configOpt = _config.SingleOrDefault(x => x.Name == name);
            return configOpt != null ? configOpt.Value : null;
        }

        /// <summary>
        /// Checks the length of the given string and ensures it always stays under 143 characters so
        /// everything fits nicely in the users chat
        /// </summary>
        /// <param name="commands"></param>
        /// <returns></returns>
        private static string ReplaceLastCommand(string commands)
        {
            if (commands.Length > 143)
            {
                commands = commands.Substring(0, commands.LastIndexOf("^1,^7", StringComparison.InvariantCulture));
            }

            return (commands.Length > 143) ? ReplaceLastCommand(commands) : commands;
        }

        /// <summary>
        /// Attempts to match the given chat message against a list of clients currently logged on to
        /// the server
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        private ServerClient DeterminePlayer(string msg)
        {
            return (from m in msg.Split(',')
                    from client in GetClients()
                    where
                        (client.ClientNum.ToString(CultureInfo.InvariantCulture) == m || client.Name.ToLower() == m ||
                         client.Name.ToLower().Contains(m))
                    select client).FirstOrDefault();
        }

        /// <summary>
        /// Generates an MD5 hash from the input string
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        private static string GetMd5Hash(string input)
        {
            MD5 md5 = new MD5CryptoServiceProvider();

            // Compute hash from the bytes of text
            md5.ComputeHash(Encoding.ASCII.GetBytes(input));

            // Get hash result after compute it
            var result = md5.Hash;
            var strBuilder = new StringBuilder();

            foreach (var t in result)
            {
                strBuilder.Append(t.ToString("x2"));
            }

            return strBuilder.ToString();
        }

        /// <summary>
        /// Converts the given timespan into a readable "hours" "minutes" "seconds" format
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        private static string GetOverallTime(TimeSpan time)
        {
            var timeLeft = new List<string>();

            var hours = Math.Abs(time.Hours);
            if (hours > 0)
            {
                timeLeft.Add(string.Format("{0} hours", hours));
            }

            var minutes = Math.Abs(time.Minutes);
            if (minutes > 0)
            {
                timeLeft.Add(string.Format("{0} minutes", minutes));
            }

            var seconds = Math.Abs(time.Seconds);
            if (seconds > 0)
            {
                timeLeft.Add(string.Format("{0} seconds", seconds));
            }

            return string.Join(" ", timeLeft.ToArray());
        }
    }

    /// <summary>
    /// A simple class used for determining when a client uses an incorrect password
    /// </summary>
    internal class FailedAttempts
    {
        public bool Locked = false;
        public DateTime Time = DateTime.MinValue;
        public int Total = 0;
    }

    /// <summary>
    /// A simple class to handle the configuration options
    /// </summary>
    internal class ConfigurationOption
    {
        public string Name, Value;
    }
}